#Compilation Guide

To compile BKEngine you will need the following prerequisites:
* [MinGW](http://sourceforge.net/projects/mingw/files/) with a GCC compiler
* MSys (Downloadable via the MinGW Installation Manager)
* [CppUnit](http://sourceforge.net/projects/cppunit/) *(Optional, only if you want to run testcases)*
* [freeglut](http://sourceforge.net/projects/freeglut/) *(Optional, only if you want to compile the OpenGLInterface)*

There are two ways of compiling BKEngine:
1. Via [Code::Blocks](http://www.codeblocks.org/downloads/26) (Be sure to get the MinGW version)
2. Using the Makefile