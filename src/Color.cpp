#include "Color.h"

namespace BK
{

    Color::Color()
    {
        a = 255;
        r = 255;
        g = 255;
        b = 255;
    }

    Color::Color(byte alpha, byte red, byte green, byte blue)
    {
        a = alpha;
        r = red;
        g = green;
        b = blue;
    }

    Color::~Color()
    {
        a = 0;
        r = 0;
        g = 0;
        b = 0;
    }

    void Color::SetAlpha(byte alpha)
    {
        a = alpha;
    }

    byte Color::GetAlpha()
    {
        return a;
    }

    void Color::SetRed(byte red)
    {
        r = red;
    }

    byte Color::GetRed()
    {
        return r;
    }

    void Color::SetGreen(byte green)
    {
        g = green;
    }

    byte Color::GetGreen()
    {
        return g;
    }

    void Color::SetBlue(byte blue)
    {
        b = blue;
    }

    byte Color::GetBlue()
    {
        return b;
    }


    void Color::SetColor(byte alpha, byte red, byte green, byte blue)
    {
        a = alpha;
        r = red;
        g = green;
        b = blue;
    }


    Pixel Color::AsARGB()
    {
        return (((Pixel)a) << (3 * BKENGINE_BYTE)) + (((Pixel)r) << (2 * BKENGINE_BYTE)) + (((Pixel)g) << (1 * BKENGINE_BYTE)) + (((Pixel)b) << (0*BKENGINE_BYTE));
    }

    Pixel Color::AsABGR()
    {
        return (a << 3 * BKENGINE_BYTE) + (b << 2 * BKENGINE_BYTE) + (g << 1 * BKENGINE_BYTE) + (r << 0*BKENGINE_BYTE);
    }

    Pixel Color::AsRGBA()
    {
        return (r << 3 * BKENGINE_BYTE) + (g << 2 * BKENGINE_BYTE) + (b << 1 * BKENGINE_BYTE) + (a << 0*BKENGINE_BYTE);
    }

    Pixel Color::AsBGRA()
    {
        return (b << 3 * BKENGINE_BYTE) + (g << 2 * BKENGINE_BYTE) + (r << 1 * BKENGINE_BYTE) + (a << 0*BKENGINE_BYTE);
    }
}
