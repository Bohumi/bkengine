#include "Texture.h"

namespace BK
{
    Texture::Texture()
    {
        w = 0;
        h = 0;
        pB.clear();
    }

    Texture::Texture(unsigned int width, unsigned int height, Pixel* pixelBuffer)
    {
        w = width;
        h = height;
        pB = pixelBuffer;
    }

    Texture::~Texture()
    {
        w = 0;
        h = 0;
        delete pB;
    }

    void Texture::SetProperties(unsigned int width, unsigned int height, Pixel* pixelBuffer)
    {
        w = width;
        h = height;
        pB = pixelBuffer;
    }

    void Texture::SetProperties(unsigned int width, unsigned int height, unsigned char* pixelBuffer)
    {
        w = width;
        h = height;
        pB = new Pixel[width*height];
        int c;
        Pixel curr = 0;
        for(int i = 0, c=0; i < width * height * 4; i++)
        {
            if(i%4==0)
            {
                c = i/4;
                pB[c] = curr;
            }
            curr = pixelBuffer[i] << (i%4);
        }
    }

    Pixel* Texture::GetPixelBuffer()
    {
        return pB;
    }

    unsigned int Texture::GetWidth()
    {
        return w;
    }

    unsigned int Texture::GetHeight()
    {
        return h;
    }

}
