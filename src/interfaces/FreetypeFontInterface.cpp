#include "interfaces/FreetypeFontInterface.h"

namespace BK
{
    FreetypeFontInterface::~FreetypeFontInterface()
    {
        // Proofs if a font with that name has been registered yet.
        for(vector<Font>::iterator it = fonts.begin(); it != fonts.end(); it++)
            DestroyFont((*it).Name);

        if(library != NULL)
            FT_Done_FreeType(library);
    }

    void FreetypeFontInterface::Init()
    {
        if(library != NULL)
        {
            FT_Done_FreeType(library);
            library = NULL;
            fonts.clear();
        }

        int error = FT_Init_FreeType(&library);

        if(error)
            throw ExternalOperationException();
    }

    void FreetypeFontInterface::RegisterFont(_IN_ string fileName,_IN_  string fontName,_IN_  size_t fontSize)
    {
        if(library == NULL)
            throw UninitializedAccessException();

        FT_Face face = NULL;

        int error = FT_New_Face(library, fileName.c_str(), 0, &face);
        if(error)
            throw ExternalOperationException();

        error = FT_Set_Pixel_Sizes(face, 100, 0);
        if(error)
            throw ExternalOperationException();

        fonts.push_back(Font(fontName, face));
    }

    Texture FreetypeFontInterface::GetStringOfFont(_IN_ string fontName, _IN_ string drawingString, _IN_ Color drawingColor)
    {
        if(library == NULL)
            throw UninitializedAccessException();

        size_t width = 0;
        size_t height = 0;

        FT_Face face = NULL;

        // Proofs if a font with that name has been registered yet.
        for(vector<Font>::iterator it = fonts.begin(); it != fonts.end(); it++)
            if((*it).Name == fontName)
                face = (*it).FontFace;
        // Otherwise an exception is thrown.
        if(face == NULL)
            throw FontNotFoundException();


        byte* pixels = NULL;
        FT_GlyphSlot slot;

        int offsetx = 0;
        int aboveBase = 0;
        int beyondBase = 0;

        int error = 0;


        // Calculates the sizes of the resulting texture in pixel.
        for(int i = 0; i < drawingString.size(); i++)
        {
            error = FT_Load_Char(face, drawingString[i], FT_LOAD_RENDER);

            if(error)
                continue;

            int top = (face->glyph->metrics.horiBearingY >> 6);
            int down = (face->glyph->metrics.height >> 6) - (face->glyph->metrics.horiBearingY >> 6);

            aboveBase = (aboveBase > top ? aboveBase : top);
            beyondBase = (beyondBase > down ? beyondBase : down);

            if(i == 0 && face->glyph->metrics.horiBearingX < 0)
                width += abs((face->glyph->metrics.horiBearingX >> 6));

            width  += ((face->glyph->advance.x >> 6));

            if((face->glyph->advance.x >> 6) < (face->glyph->metrics.width >> 6) + (face->glyph->metrics.horiBearingX >> 6))
                width += (face->glyph->metrics.width >> 6) + (face->glyph->metrics.horiBearingX >> 6) - (face->glyph->advance.x >> 6);
        }

        pixels = new byte[width * (height = (aboveBase + beyondBase))];

        // Assembles the resulting texture out of every single character in it.
        for(int i = 0; i < drawingString.size(); i++)
        {
            error = FT_Load_Char(face, drawingString[i], FT_LOAD_RENDER);

            if(error)
                continue;

            unsigned char* bmp = (unsigned char*)face->glyph->bitmap.buffer;
            int yoffset = aboveBase - (face->glyph->metrics.horiBearingY>>6);

            for(int row = 0; row < face->glyph->bitmap.rows; row++)
                for(int col = 0; col < face->glyph->bitmap.width; col++)
                {
                    if(row == 0 && col == 0 && i == 0 && face->glyph->metrics.horiBearingX < 0)
                        offsetx += abs((face->glyph->metrics.horiBearingX >> 6));

                    pixels[(yoffset + row) * width + col + offsetx + (face->glyph->metrics.horiBearingX >> 6)] += bmp[row * face->glyph->bitmap.width + col];
                }

            offsetx += face->glyph->advance.x >> 6;
        }

        Color c = Color(drawingColor);

        vector<Pixel> buffer;
        // Mapping of the string texture with the given color.
        for(int i = 0; i < width * height; i++)
            buffer.push_back((Pixel)((pixels[i]) + c.AsRGBA() - c.GetAlpha( )));
        delete pixels;

        return Texture(width, height, vector<Pixel>(buffer));
    }

    void FreetypeFontInterface::DestroyFont(_IN_ string fontName)
    {
        if(library == NULL)
            throw UninitializedAccessException();

        FT_Face face = NULL;

        // Proofs if a font with that name has been registered yet.
        for(vector<Font>::iterator it = fonts.begin(); it != fonts.end(); it++)
            if((*it).Name == fontName)
                face = (*it).FontFace;
        // Otherwise an exception is thrown.
        if(face == NULL)
            throw FontNotFoundException();

        FT_Done_Face(face);
    }
}
