#include "interfaces/OpenGLInterface.h"
#include <stdio.h>

namespace BK
{
    vector<IRenderAble*> OpenGLInterface::renderAbles;
    GLInterface* OpenGLInterface::gl;

    void OpenGLInterface::CreateContext()
    {
        glutInit(new int(0),NULL);

        glutInitContextVersion( 2, 1 );
    }


    void OpenGLInterface::Run()
    {
        gl = this;
        glutMainLoop();
    }

    void OpenGLInterface::SwapBuffersFunction()
    {
        glClear(GL_COLOR_BUFFER_BIT);
        for(int i = 0; i < renderAbles.size(); i++)
            (renderAbles[i])->Render(gl);
        glutSwapBuffers();
    }

    void OpenGLInterface::SetLoopFunction(void (*loopFunction)(int))
    {
        glutTimerFunc( 0, loopFunction, 1);
    }

    void OpenGLInterface::SetDisplayFunction(void (*displayFunction)())
    {
        glutDisplayFunc(displayFunction);
    }

    void OpenGLInterface::AddRenderAble(IRenderAble* ra)
    {
        renderAbles.push_back(ra);
    }

    void OpenGLInterface::CreateWindow(_IN_ int x, _IN_ int y, _IN_ int width, _IN_ int height, _IN_ std::string title)
    {
        if(x < 0 || y < 0 || width < 0 || height < 0)
            throw ArgumentException();
        //Create Double Buffered Window
        glutInitDisplayMode( GLUT_DOUBLE );
        glutInitWindowPosition(x,y);
        glutInitWindowSize(width, height);

        windowId = glutCreateWindow(title.c_str());
        glViewport( 0.f, 0.f, width, height);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        glOrtho( 0.0, width, height, 0.0, -1.0, 1.0 );

        glMatrixMode( GL_MODELVIEW );
        glLoadIdentity();

        glClearColor( 0.5f, 0.5f, 0.5f, 0.5f );

        glEnable( GL_TEXTURE_2D );
        glEnable( GL_LINE_SMOOTH );
        glEnable( GL_BLEND );

        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    }



    void OpenGLInterface::DestroyWindow()
    {
        if(windowId == -1)
        {
            throw WindowNotCreatedException();
        }
        glutDestroyWindow(windowId);
    }

    void OpenGLInterface::DestroyContext()
    {
        glutExit();
    }

    void OpenGLInterface::DrawTexture(Texture t)
    {
        glLoadIdentity();
        glBindTexture(GL_TEXTURE_2D, t.GetID());
        glBegin( GL_QUADS );
            glTexCoord2f( 0.f, 0.f ); glVertex2f( 0.f,          0.f          );
            glTexCoord2f( 1.f, 0.f ); glVertex2f( t.GetWidth(), 0.f          );
            glTexCoord2f( 1.f, 1.f ); glVertex2f( t.GetWidth(), t.GetHeight() );
            glTexCoord2f( 0.f, 1.f ); glVertex2f( 0.f,          t.GetHeight());
        glEnd();
    }

    void OpenGLInterface::RegisterTexture(Texture* tex)
    {
        int id;
        glGenTextures(1, (GLuint*)&id);
        tex->SetID(id);
        glBindTexture(GL_TEXTURE_2D,id);

        GLuint* pixels = (GLuint*)tex->GetPixelBuffer().data();
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, tex->GetWidth(), tex->GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );

        glBindTexture(GL_TEXTURE_2D, NULL);
    }
}
