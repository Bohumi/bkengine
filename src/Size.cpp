#include "Size.h"

namespace BK
{
    Size::Size()
    {
        w = 0;
        h = 0;
    }

    Size::Size(size_t width, size_t height)
    {
        w = width;
        h = height;
    }

    Size::~Size()
    {
        w = 0;
        h = 0;
    }


    size_t Size::GetWidth()
    {
        return w;
    }

    void Size::SetWidth(size_t width)
    {
        w = width;
    }

    size_t Size::GetHeight()
    {
        return h;
    }

    void Size::SetHeight(size_t height)
    {
        h = height;
    }
}
