/** \file GLInterface.h
 * This class serves as an interface for custom graphics library interfaces.
 *
 * If you wish to make BKEngine work with your own graphics library,
 * you will have to create a class which derives from this one.
 *
 * \author Christoph B�hmwalder <christoph@boehmwalder.at>
 * \date 2014
 */

#ifndef GLINTERFACE_H
#define GLINTERFACE_H

#include <string>
#include "Utilities.h"

namespace BK
{
    class GLInterface
    {
        public:
            /** \brief
             * This method should be used to initialize your graphics library.
             *
             * If your graphics library of choice does not need initialization
             * (e.g. you are writing to memory directly), you don't have to
             * implement this method.
             * \return void
             *
             */
            virtual void CreateContext(){};

            /** \brief
             * This method should spawn a window to display graphics in.
             *
             * If your graphics library does not need or support a window, you
             * may leave this method unimplemented.
             *
             * \param[in] x The x position where the window should be placed
             * \param[in] y The y position where the window should be placed
             * \param[in] width The desired width of the window
             * \param[in] height The height of the window
             * \param[in] title The string displayed in the title bar of the window
             * \return void
             *
             */
            virtual void CreateWindow(_IN_ int x, _IN_ int y, _IN_ int width, _IN_ int height, _IN_ std::string title){}


            /** \brief
             * This method should destroy the window spawned by CreateWindow
             * \return void
             *
             */
            virtual void DestroyWindow(){}

            /** \brief
             * This method should be used to free all the resources used by your graphics library and terminate it.
             *
             * If your graphics library does not have any resources to free,
             * you don't have to implement this method.
             * \return void
             *
             */
            virtual void DestroyContext(){};

            /** \brief
             * This method should be used to draw a texture to the graphics buffer.
             *
             * A common approach to implementing this method would be interpreting
             * the texture's pixel buffer and drawing it to your buffer.
             * \param[in] texture The texture that should be drawn to the screen. You may not modify this parameter.
             * \return void
             *
             */
            virtual void RegisterTexture(Texture* texture) = 0;



            /** \brief
             * This method should be used to draw a texture to the graphics buffer.
             *
             * A common approach to implementing this method would be interpreting
             * the texture's pixel buffer and drawing it to your buffer.
             * \param[in] texture The texture that should be drawn to the screen. You may not modify this parameter.
             * \return void
             *
             */
            virtual void DrawTexture(Texture texture) = 0;

            /** \brief
             * Returns whether double buffering is enabled
             * \return bool Whether double buffering is enabled
             *
             */
            bool IsDoubleBuffered() { return doubleBuffered; }

            /** \brief
             * Enables double buffering for this instance
             * \return void
             *
             */
            void EnableDoubleBuffering() { doubleBuffered = true; }

            /** \brief
             * Disables double buffering for this instance
             * \return void
             *
             */
            void DisableDoubleBuffering() { doubleBuffered = false; }

            virtual void SetLoopFunction(void (*loopFunction)(int)) = 0;
            virtual void SetDisplayFunction(void (*displayFunction)()) = 0;

            void Run();
        protected:
            bool doubleBuffered;

        private:



    };
}
#endif // GLINTERFACE_H
