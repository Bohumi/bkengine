/** \file FontInterface.h
 * This class provides an interface for handling fonts.
 *
 * The BKEngine is able to access font files like .ttf through this interface.
 * You have to derive and implement your own font interface if you want to work with fonts!
 *
 * \author Sebastian Kaupper <kauppersebastian@gmail.com>
 * \date 2014
 */

#ifndef FONTINTERFACE_H
#define FONTINTERFACE_H

#include "Utilities.h"

namespace BK
{
    class FontInterface
    {
        public:
            FontInterface(){}
            virtual ~FontInterface(){}

            /** \brief
             * This method should be used to initialize a font.
             *
             * To work with fonts afterwards you have to register it with the path of the file,
             * the font size you want to work with and the name you want to call the font in the later project.
             * You will have to store the different fonts in a specific way, care about it!
             *
             * \param[in] fileName The name of the font file to use.
             * \param[in] fontName The name to store the font.
             * \param[in] fontSize The size with which the font must be initialized.
             * \return void
             *
             */
            virtual void RegisterFont(_IN_ string fileName, _IN_ string fontName, _IN_ size_t fontSize)=0;

            /** \brief
             * This method should generate a texture out of a before registered font.
             *
             * To display a font using BKEngine, you need to extract a Texture out of a font file and a given string.
             * In addition you are able to pass a color which the resulting texture should have.
             *
             * \param[in] fontName The name of the stored font.
             * \param[in] drawingString A string which should be returned as a texture.
             * \param[in] drawingColor The color the resulting texture have to have.
             * \return void
             *
             */
            virtual Texture GetStringOfFont(_IN_ string fontName, _IN_ string drawingString, _IN_ Color drawingColor)=0;

            /** \brief
             * This method should be used to destroy stored fonts when they are not needed anymore.
             *
             * In order to allocate as less memory as you really need you can destroy a already registered font
             * based on its name.
             *
             * \param[in] fontName The name of the stored font to be destroyed.
             * \return void
             *
             */
            virtual void DestroyFont(_IN_ string fontName)=0;
    };
}

#endif //FONTINTERFACE_H
