#ifndef COLOR_H_INCLUDED
#define COLOR_H_INCLUDED

#include "Utilities.h"

namespace BK
{
    class Color
    {
    public:
        Color();
        Color(byte alpha, byte red, byte green, byte blue);
        virtual ~Color();

        void SetAlpha(byte alpha);
        byte GetAlpha();
        void SetRed(byte red);
        byte GetRed();
        void SetGreen(byte green);
        byte GetGreen();
        void SetBlue(byte blue);
        byte GetBlue();

        void SetColor(byte alpha, byte red, byte green, byte blue);

        Pixel AsARGB();
        Pixel AsABGR();
        Pixel AsRGBA();
        Pixel AsBGRA();
    protected:
    private:
        byte a;
        byte r;
        byte g;
        byte b;
    };
}

#endif // COLOR_H_INCLUDED
