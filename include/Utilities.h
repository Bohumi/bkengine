#ifndef UTILITIES_H_INCLUDED
#define UTILITIES_H_INCLUDED

#include <iostream>
#include <vector>
#define cimg_use_png
#define cimg_convert_path "gm convert"
#include <CImg.h>
#undef LoadImage

using namespace std;
using namespace cimg_library;

#define _IN_ const
#define _OUT_
#define _INOUT_

#define BKENGINE_BYTE 8

namespace BK
{

    typedef uint32_t Pixel;
    typedef uint8_t byte;
}

#include "Size.h"
#include "Color.h"
#include "Texture.h"

#endif // UTILITIES_H_INCLUDED
