#ifndef EXAMPLERENDERABLE_H_INCLUDED
#define EXAMPLERENDERABLE_H_INCLUDED

#include "IRenderAble.h"
#include "interfaces/FreetypeFontInterface.h"

namespace BK
{
    class ExampleRenderAble : public IRenderAble
    {
    public:
        ExampleRenderAble(Texture t) : IRenderAble() { texture = t;}
        ~ExampleRenderAble() {}

        virtual int Render(GLInterface* gl)
        {
            gl->DrawTexture(texture);
            return 0;
        }

        Texture texture;
    };
}


#endif // EXAMPLERENDERABLE_H_INCLUDED
