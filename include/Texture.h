#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include "Utilities.h"

namespace BK
{
    class Texture
    {
    public:
        Texture();
        Texture(size_t width, size_t height, vector<Pixel> pixelBuffer);
        virtual ~Texture();

        void SetProperties(size_t width, size_t height, vector<Pixel> pixelBuffer);
        void SetProperties(size_t width, size_t height, unsigned char* pixelBuffer);

        vector<Pixel> GetPixelBuffer();
        size_t GetWidth();
        size_t GetHeight();

        void SetID(int i) {id = i;}
        int GetID(){return id;}

    private:
        size_t w;
        size_t h;
        vector<Pixel>  pB;

        int id;
    };
}

#endif // TEXTURE_H_INCLUDED
