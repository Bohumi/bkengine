#ifndef OPENGLINTERFACE_H
#define OPENGLINTERFACE_H

#include "Utilities.h"
#undef CreateWindow
#include "GLInterface.h"
#include <exceptions/WindowNotCreatedException.h>
#include <exceptions/ArgumentException.h>
#include <GL/freeglut.h>
#include <sstream>
#include "IRenderAble.h"

namespace BK
{
    class OpenGLInterface : public GLInterface
    {
        public:
            void CreateContext();
            void CreateWindow(_IN_ int x, _IN_ int y, _IN_ int width, _IN_ int height, _IN_ std::string title);

            void DestroyWindow();
            void DestroyContext();

            void AddRenderAble(IRenderAble*);
            void RegisterTexture(Texture*);
            void DrawTexture(Texture texture);


            void SetLoopFunction(void (*loopFunction)(int));
            void SetDisplayFunction(void (*displayFunction)());

            void Run();

            void ClearColor(Color color);
        protected:
        private:
            static void SwapBuffersFunction();
            static void TimerFunction(int value);

            static vector<IRenderAble*> renderAbles;
            static GLInterface* gl;

            int windowId = -1;
    };
}
#endif // OPENGLINTERFACE_H
