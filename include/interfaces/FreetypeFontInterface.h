/** \file FreetypeFontInterface.h
 * This class provides an implementation of the font interface using freetype.
 *
 * With this class you can use most types of font files to generate textures
 * out of strings under windows. This is only a sample implementation.
 *
 * \author Sebastian Kaupper <kauppersebastian@gmail.com>
 * \date 2014
 */

#ifndef FREETYPEFONTINTERFACE_H_INCLUDED
#define FREETYPEFONTINTERFACE_H_INCLUDED

#include "utilities.h"
#include "FontInterface.h"

#include "exceptions/UninitializedAccessException.h"
#include "exceptions/ExternalOperationException.h"
#include "exceptions/FontNotFoundException.h"


#include "freetype/ft2build.h"
#include FT_FREETYPE_H

namespace BK
{
    struct Font
    {
        Font(string n, FT_Face f) : Name(n), FontFace(f) {}
        string Name;
        FT_Face FontFace;
    };

    class FreetypeFontInterface : public FontInterface
    {
        public:
            FreetypeFontInterface() : FontInterface(), library(NULL){}
            virtual ~FreetypeFontInterface();

            void Init();
            virtual void RegisterFont(_IN_ string fileName, _IN_ string fontName, _IN_ size_t fontSize);
            virtual Texture GetStringOfFont(_IN_ string fontName, _IN_ string drawingString, _IN_ Color drawingColor);
            virtual void DestroyFont(_IN_ string fontName);
        private:
            FT_Library library;
            vector<Font> fonts;
    };
}


#endif // FREETYPEFONTINTERFACE_H_INCLUDED
