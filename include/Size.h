#ifndef SIZE_H_INCLUDED
#define SIZE_H_INCLUDED

#include "Utilities.h"

namespace BK
{
    class Size
    {
    public:
        Size();
        Size(size_t width, size_t height);
        virtual ~Size();

        size_t GetWidth();
        void SetWidth(size_t width);

        size_t GetHeight();
        void SetHeight(size_t height);

    private:
        size_t w;
        size_t h;
    };

}

#endif // SIZE_H_INCLUDED
