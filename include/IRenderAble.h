#ifndef IRENDERABLE_H_INCLUDED
#define IRENDERABLE_H_INCLUDED

namespace BK
{
    class IRenderAble
    {
    public:
        IRenderAble(){}
        virtual ~IRenderAble(){};

        virtual int Render(GLInterface* gl)=0;
    };
}

#endif // IRENDERABLE_H_INCLUDED
