#ifndef UNINITIALIZEDACCESSEXCEPTION_H_INCLUDED
#define UNINITIALIZEDACCESSEXCEPTION_H_INCLUDED

#include <stdexcept>
using std::runtime_error;

namespace BK
{
    class UninitializedAccessException : public runtime_error
    {
    public:
        UninitializedAccessException() : runtime_error( "The object to use is still empty!"){}
    };
}

#endif // UNINITIALIZEDACCESSEXCEPTION_H_INCLUDED
