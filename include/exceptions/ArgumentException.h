#ifndef ARGUMENTEXCEPTION_H_INCLUDED
#define ARGUMENTEXCEPTION_H_INCLUDED

#include <stdexcept>
using std::runtime_error;

namespace BK
{
    class ArgumentException : public runtime_error
    {
    public:
        ArgumentException() : runtime_error( "The arguments for this function call were invalid"){}
    };
}


#endif // ARGUMENTEXCEPTION_H_INCLUDED
