#ifndef EXTERNALOPERATIONEXCEPTION_H_INCLUDED
#define EXTERNALOPERATIONEXCEPTION_H_INCLUDED

#include <stdexcept>
using std::runtime_error;

namespace BK
{
    class ExternalOperationException : public runtime_error
    {
    public:
        ExternalOperationException() : runtime_error( "An error occured while calling a third-party function!"){}
    };
}

#endif // EXTERNALOPERATIONEXCEPTION_H_INCLUDED
