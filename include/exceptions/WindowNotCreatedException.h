#ifndef OPENGLEXCEPTION_H
#define OPENGLEXCEPTION_H

#include <stdexcept>
using std::runtime_error;

namespace BK
{
    class WindowNotCreatedException : public runtime_error
    {
    public:
        WindowNotCreatedException() : runtime_error( "This function cannot be called before the window is created"){}
    };
}
#endif // OPENGLEXCEPTION_H
