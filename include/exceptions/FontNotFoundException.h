#ifndef FONTNOTFOUNDEXCEPTION_H_INCLUDED
#define FONTNOTFOUNDEXCEPTION_H_INCLUDED

#include <stdexcept>
using std::runtime_error;

namespace BK
{
    class FontNotFoundException : public runtime_error
    {
    public:
        FontNotFoundException() : runtime_error( "The font has not been registered yet!"){}
    };
}

#endif // FONTNOTFOUNDEXCEPTION_H_INCLUDED
