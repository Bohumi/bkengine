#include <iostream>
#include <interfaces/OpenGLInterface.h>
#include <interfaces/FreetypeFontInterface.h>
#include "example/ExampleRenderAble.h"
#include <windows.h>

using namespace std;
using namespace BK;
using namespace cimg_library;

int main(int argc, char** argv)
{
    OpenGLInterface* o = new OpenGLInterface();
    o->CreateContext();
    o->CreateWindow(0,0,800,600,"sers");
    FreetypeFontInterface* f = new FreetypeFontInterface();
    f->Init();
    f->RegisterFont("calibri.ttf", "calibri", 10);

    Color c;
    c.SetColor(255, 255, 0, 0);

    Texture t = f->GetStringOfFont("calibri", "testtesttest", c);
    o->RegisterTexture(&t);


    ExampleRenderAble* ex = new ExampleRenderAble(t);
    o->AddRenderAble(ex);


    o->Run();
    return 0;
}
